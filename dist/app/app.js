'use strict';

var app = angular.module("Project", [
	"ngCookies",
  'ui.router',
  
	'Project.Controller',
  'Project.Service'
]);

app.config(function() {
	
});