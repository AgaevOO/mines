var commonController = angular.module('Project.Controller', ['ui.router']);

commonController.controller('Ctrl', ['$scope', '$rootScope', '$timeout', '$cookies', '$http', 'httpService', function($scope, $rootScope, $timeout, $cookies, $http, httpService) {

  console.log('Start!');

  $scope.setServerData = function(url, input) {
    httpService.post(url, input, function(data) {
      console.log('Success load: ' + data);
    });
  }

  function getServerData(url) {
    httpService.get(url, function(data) {
      console.log(data);
    });
  }

  $scope.moves = [];

  var N = 10;
  var id = 0;

  var field = {
    "height": N, 
    "width": N, 
    "count": N
  }

  $scope.startNewGame = function() {
    httpService.post('games/', field, function(data) {
      $scope.id = data.game.id;
    });
  }

  $scope.move = function(cell) {
    var x = cell % 10;
    var y = Math.floor(cell/10);
    var coords = {x: x, y: y};
    httpService.post('games/' + $scope.id + '/reveal', coords, function(data) {
      for (var i = data.cells.length - 1; i >= 0; i--) {
        var number = data.cells[i].x + data.cells[i].y * 10;
        $scope.moves[number] = data.cells[i].count;
        if (data.cells[i].count == -1) {
          //alert('Game over!');
        }
      }
    });
  }

  $scope.revealAllClear = function() {
   for (var cell = 0; cell < 100; cell++) {
     if ($scope.moves[cell] === undefined) {
      var x = cell % 10;
      var y = Math.floor(cell/10);
      var coords = {x: x, y: y};
      httpService.post('games/' + $scope.id + '/reveal', coords, function(data) {
        for (var i = data.cells.length - 1; i >= 0; i--) {
          var number = data.cells[i].x + data.cells[i].y * 10;
          $scope.moves[number] = data.cells[i].count;
        }
        $scope.revealAllClear();
      });
      break;
     }
   }
  }

  $scope.revealAllDirty = function() {
    for (var i = 0; i < $scope.cells.length; i++) {
        $scope.move($scope.cells[i]);
    }
  }

  $scope.startNewGame();
  $scope.cells = Array.from(Array(field.height * field.width).keys());

}]);