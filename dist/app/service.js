var commonService = angular.module('Project.Service', ['ui.router']);

commonService.service('httpService', ['$http', '$rootScope', function($http, $rootScope) {

  this.get = function(url, successCallback, errorCallback) {
    this.sendHttpRequest('GET', url, null, successCallback, errorCallback ? errorCallback : function(data){console.log('ERROR!: ' + data.statusText + '(' + data.status + ')')});
  };

  this.post = function(url, data, successCallback, errorCallback) {
    this.sendHttpRequest('POST', url, data, successCallback, errorCallback ? errorCallback : function(data){console.log('ERROR!: ' + data.statusText + '(' + data.status + ')')});
  };

  this.sendHttpRequest = function(method, url, data, successCallback, errorCallback) {
    $http({
      method : method,
      url : 'https://fathomless-peak-55967.herokuapp.com/' + url,
      data : data
    })
    .then(
      function(data) {
        successCallback(data.data);
      }, 
      function(data) {
        errorCallback(data);
      }
     );
  }

}]);